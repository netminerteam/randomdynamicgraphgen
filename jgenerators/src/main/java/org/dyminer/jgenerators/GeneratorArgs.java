package org.dyminer.jgenerators;

import java.io.File;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;

import org.kohsuke.args4j.Option;

public class GeneratorArgs {
	
	@Option(name="-n", usage="the maximum number of nodes in a graph")
	@Min(2)
	protected int maxNodeCount = 30;
	
	@Option(name="-l", usage="the maximum number of possible edges")
	@Min(1)
	protected int maxLabelCount = 1;

	@Option(name="-sr", usage="the streaming rate of the dataset in graphs/hour")
	@Min(1)
	protected int streamingRate = 60;
	
	@Option(name="-pn", usage="positive perturbation probability, probability of adding new edges")
	@Min(0) @Max(1)
	protected float positivePerturbation = 0.025f;
	
	@Option(name="-nn", usage="negative perturbation probability, probability of removing old edges")
	@Min(0) @Max(1)
	protected float negativePerturbation = 0.025f;
	
	@Option(name="-start", usage="start date time (yyyy-MM-dd hh:mm:ss, default current datetime truncated to hour)", depends={"-end"})
	@Pattern(regexp = "([0-9]{4}-(0[1-9]|1[0-2])-[0-2][0-9]|3[0-1])T[0-2][0-3]:[0-5][0-9]:[0-5][0-9]")
	protected String startDateTime = null;
	
	@Option(name="-end", usage="end date time (yyyy-MM-dd hh:mm:ss, default current datetime plus 1 hour)", depends={"-start"})
	@Pattern(regexp = "([0-9]{4}-(0[1-9]|1[0-2])-[0-2][0-9]|3[0-1])T[0-2][0-3]:[0-5][0-9]:[0-5][0-9]")
	protected String endDateTime = null;
	
	@Option(name="-s", usage="seed number to be used in every random procedure")
	protected long seed = 10;

	@Option(name="-o", usage="output file path", required=true)
	protected File outputFile;

	public int getMaxNodeCount() {
		return maxNodeCount;
	}

	public int getMaxLabelCount() {
		return maxLabelCount;
	}

	public int getStreamingRate() {
		return streamingRate;
	}

	public float getPositivePerturbation() {
		return positivePerturbation;
	}

	public float getNegativePerturbation() {
		return negativePerturbation;
	}

	public File getOutputFile() {
		return outputFile;
	}
	
	public long getSeed() {
		return seed;
	}
	
	public Instant getStartDate() {
		Instant result = null;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")
			.withZone(ZoneOffset.UTC);
		if(this.startDateTime!=null) {
			TemporalAccessor formatted = formatter.parse(this.startDateTime);
			result = Instant.from(formatted);
		}
		return result;
	}
	
	public Instant getEndDate() {
		Instant result = null;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")
			.withZone(ZoneOffset.UTC);
		if(this.endDateTime!=null) {
			TemporalAccessor formatted = formatter.parse(this.endDateTime);
			result = Instant.from(formatted);
		}
		return result;
	}
}