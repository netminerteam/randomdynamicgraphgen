package org.dyminer.jgenerators;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.LinkedList;
import java.util.Random;
import java.util.Vector;
import java.util.stream.Stream;

import org.dyminer.model.TemporalEdge;
import org.graphstream.algorithm.generator.BarabasiAlbertGenerator;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.MultiGraph;

public class GeneratorUtils {

	public static Stream<TemporalEdge> singlePassGeneration(GeneratorArgs args){
		//we initialize the list which will contains the generated content
		LinkedList<TemporalEdge> edges = new LinkedList<>();
		
		//randomness setting (full deterministic)
		long seed = args.getSeed();
		Random dice = new Random(seed);

		//generates the initial network snapshot by using a scale-free generative
		//model with given seed (fixed to 10). The model will create a graph with
		//a number of nodes equals to the half of the maximum number of nodes.
		Graph graph = new MultiGraph("grafo", false, true);
		BarabasiAlbertGenerator generator = new BarabasiAlbertGenerator(3);
		int initialNodeCount = (args.getMaxNodeCount()-1)/2;
		generator.setDirectedEdges(true, true);
		generator.addSink(graph);
		generator.setRandomSeed(seed);
		generator.begin();
		for(int i=1; i<initialNodeCount; i++) {
			generator.nextEvents();
		}
		generator.end();

		//assignment of labels to edges
		for(Edge edge : graph.getEdgeSet()) {
			edge.setAttribute("label", 0);
		}

		//assignment of labels to nodes
		for(Node node : graph.getNodeSet()) {
			node.setAttribute("label", node.getId());
		}

		//we will now reverse engineer the generated snapshot in order to generate the data stream.
		int tripleCount = args.getMaxLabelCount() * args.getMaxNodeCount() * (args.getMaxNodeCount()-1);
		Vector<Boolean> prototype = new Vector<Boolean>(tripleCount);
		for(int i=0; i<tripleCount; i++) {
			prototype.add(false);
		}
		for(Edge edge : graph.getEdgeSet()) {
			Integer n1Idx = Integer.parseInt(edge.getNode0().getId());
			Integer n2Idx = Integer.parseInt(edge.getNode1().getId());
			Integer labelIdx = edge.getAttribute("label");
			int bitIndex = GeneratorUtils.edgeToIndex(args, n1Idx, n2Idx, labelIdx);
			prototype.set(bitIndex, true);
		}

		//once created the prototype vector, we create the stream starting from this immutable content
		Instant startDateTime = args.getStartDate();
		Instant endDateTime = args.getEndDate();
		long millisInInterval = 0;
		if(startDateTime==null || endDateTime==null) {
			startDateTime = Instant.now().truncatedTo(ChronoUnit.HOURS);
			endDateTime = startDateTime.plus(1, ChronoUnit.HOURS);
			millisInInterval = 3600000;
		}else {
			millisInInterval = endDateTime.toEpochMilli() - startDateTime.toEpochMilli();
		}

		long millisDelta = millisInInterval/args.getStreamingRate();
		long iterations = millisInInterval/millisDelta;
		Instant currentHour = Instant.from(startDateTime);
		for(int iteration=0; iteration<iterations; iteration++) {
			Vector<Boolean> vector = new Vector<Boolean>(prototype);

			//we perturbate the current vector
			for(int index=0; index<vector.size(); index++) {
				float value = dice.nextFloat();
				if(vector.get(index)==true) {
					//throw dice with negative probability
					if(value < args.getNegativePerturbation()) {
						vector.set(index, false);
					}
				}else {
					//throw dice with positive probability
					if(value < args.getPositivePerturbation()) {
						vector.set(index, true);
					}
				}
			}

			//we convert the vector to a temporal graph object
			for(int index=0; index<vector.size(); index++) {
				if(vector.get(index)) {
					//indices
					int startNodeIdx = GeneratorUtils.indexToStartNodeIdx(args, index);
					int endNodeIdx = GeneratorUtils.indexToEndNodeIdx(args, index);
					int labelIdx = GeneratorUtils.indexToLabelIdx(args, index);

					//labels
					String startNode = "n"+Integer.toString(startNodeIdx);
					String endNode = "n"+Integer.toString(endNodeIdx);
					String label = "label"+Integer.toString(labelIdx);
					TemporalEdge edge = new TemporalEdge(Date.from(currentHour), startNode, endNode, label);
					edges.add(edge);
				}
			}
			
			//increments the timestamp of delta milliseconds
			currentHour = currentHour.plusMillis(millisDelta);
		}
		
		return edges.stream();
	}
	
	private static int edgeToIndex(GeneratorArgs args, int n1Idx, int n2Idx, int labelIdx) {
		return n1Idx + (n2Idx * args.getMaxNodeCount()) + 
			(labelIdx * args.getMaxNodeCount() * args.getMaxLabelCount());
	}
	
	private static int indexToStartNodeIdx(GeneratorArgs args, int index) {
		int z = index / (args.getMaxNodeCount() * args.getMaxLabelCount());
		index -= (z * args.getMaxNodeCount() * args.getMaxLabelCount());
		return index % args.getMaxNodeCount();
	}
	
	private static int indexToEndNodeIdx(GeneratorArgs args, int index) {
		int z = index / (args.getMaxNodeCount() * args.getMaxLabelCount());
		return z;
	}
	
	private static int indexToLabelIdx(GeneratorArgs args, int index) {
		int z = index / (args.getMaxNodeCount() * args.getMaxLabelCount());
		index -= (z * args.getMaxNodeCount() * args.getMaxLabelCount());
		int value = index / args.getMaxNodeCount();
		return value;
	}
}
