package org.dyminer.jgenerators;

import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.OptionDef;
import org.kohsuke.args4j.spi.DelimitedOptionHandler;
import org.kohsuke.args4j.spi.LongOptionHandler;
import org.kohsuke.args4j.spi.Setter;

public class MultiLongOptionHandler extends DelimitedOptionHandler<Long> {
    public MultiLongOptionHandler(CmdLineParser parser, OptionDef option, Setter<? super Long> setter) {
        super(parser, option, setter, ",", new LongOptionHandler(parser, option, setter));
    }
}