package org.dyminer.jgenerators;

import java.io.File;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;

import org.kohsuke.args4j.Option;

public class MultipleGeneratorArgs {
	
	@Option(name="-b", usage="the length of the generated stream in terms of number of blocks")
	@Min(2)
	private int phasesCount = 2;
	
	@Option(name="-n", usage="the maximum number of nodes in a graph")
	@Min(2)
	private int maxNodeCount = 30;
	
	@Option(name="-l", usage="the maximum number of possible edges")
	@Min(1)
	private int maxLabelCount = 1;

	@Option(name="-sr", usage="the streaming rate of the dataset in graphs/hour")
	@Min(1)
	private int streamingRate = 60;
	
	@Option(name="-pn", usage="positive perturbation probability, probability of adding new edges")
	@Min(0) @Max(1)
	private float positivePerturbation = 0.025f;
	
	@Option(name="-nn", usage="negative perturbation probability, probability of removing old edges")
	@Min(0) @Max(1)
	private float negativePerturbation = 0.025f;
	
	@Option(name="-start", usage="start date time (yyyy-MM-dd hh:mm:ss, default current datetime truncated to hour)")
	@Pattern(regexp = "([0-9]{4}-(0[1-9]|1[0-2])-[0-2][0-9]|3[0-1])T[0-2][0-3]:[0-5][0-9]:[0-5][0-9]")
	private String startDateTime = null;
	
	@Option(name="-s", usage="seed number to be used in every random procedure")
	private long seed = 10;
	
	@Option(name="-sl", usage="the number of distinct seeds to be used along the random procedures", required=true, handler = MultiLongOptionHandler.class)
	private List<Long> seeds = new ArrayList<Long>();
	
	@Option(name="-repl", usage="seed chosen with replacement (default false)")
	protected boolean withReplacement = false;

	@Option(name="-o", usage="output file path", required=true)
	private File outputFile;
	
	@Option(name="-gt", usage="output ground truth", required=true)
	private File outputGroundTruth;
	
	@Option(name="-bs", usage="block size", required=true)
	private int blockSize;
	
	public int getPhaseCount() {
		return phasesCount;
	}

	public int getMaxNodeCount() {
		return maxNodeCount;
	}

	public int getMaxLabelCount() {
		return maxLabelCount;
	}

	public int getStreamingRate() {
		return streamingRate;
	}
	
	public int getBlockSize() {
		return blockSize;
	}

	public float getPositivePerturbation() {
		return positivePerturbation;
	}

	public float getNegativePerturbation() {
		return negativePerturbation;
	}

	public File getOutputFile() {
		return outputFile;
	}
	
	public File getOutputGroundTruthFile() {
		return outputGroundTruth;
	}
	
	public List<Long> getPhaseSeedList(){
		return seeds;
	}
	
	public long getSeed() {
		return seed;
	}
	
	public boolean withReplacement() {
		return withReplacement;
	}
	
	public Instant getStartDate() {
		Instant result = null;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")
			.withZone(ZoneOffset.UTC);
		if(this.startDateTime!=null) {
			TemporalAccessor formatted = formatter.parse(this.startDateTime);
			result = Instant.from(formatted);
		}
		return result;
	}
}
