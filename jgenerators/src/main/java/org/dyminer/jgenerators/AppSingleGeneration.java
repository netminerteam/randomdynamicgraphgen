package org.dyminer.jgenerators;

import java.io.FileWriter;
import java.io.IOException;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.dyminer.model.TemporalEdge;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.OptionHandlerFilter;

public class AppSingleGeneration implements Runnable{
	
	private GeneratorArgs args;
	
	public AppSingleGeneration(GeneratorArgs args) {
		this.args = args;
	}
	
	
	public void run() {
		FileWriter writer;
		try {
			//we open the file
			writer = new FileWriter(this.args.getOutputFile());
		
			//we get the generated content
			List<TemporalEdge> edges = GeneratorUtils.singlePassGeneration(args)
				.collect(Collectors.toList());

			//prints the csv
			this.printCSV(writer, edges);
		
			//we close the file
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	private void printCSV(FileWriter writer, List<TemporalEdge> edges) throws IOException {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:S -0000")
			.withZone(ZoneOffset.UTC);
		
		for(TemporalEdge edge : edges) {
			StringBuilder builder = new StringBuilder();
			builder.append(formatter.format(edge.getTimestamp()));
			builder.append(", ");
			builder.append(edge.getStartNode());
			builder.append(", ");
			builder.append(edge.getLabel());
			builder.append(", ");
			builder.append(edge.getEndNode());
			builder.append("\n");
			writer.write(builder.toString());
		}
	}
	
	
	
	
	/**
	 * Application entry point, in this static method
	 * we perform command-line argument parsing and validation.
	 * If validation succeed, we launch the detector on the designated
	 * data stream.
	 * 
	 * @param args
	 */
	public static void main(String[] args){
		Logger.getLogger("org.hibernate").setLevel(Level.SEVERE);
		final GeneratorArgs arguments = new GeneratorArgs();
		final CmdLineParser argsParser = new CmdLineParser(arguments);

		try {
			argsParser.parseArgument(args);
			System.out.println("generating stream {sr="+arguments.getStreamingRate()+
				" (graphs/hour), |V|="+arguments.getMaxNodeCount()+
				", |L|="+arguments.getMaxLabelCount()+"}");
			new AppSingleGeneration(arguments).run();
		} catch (CmdLineException e) {
			System.err.println(e.getMessage());
			System.err.println("java SampleMain [options...] arguments...");
			argsParser.printUsage(System.err);
			System.err.println();
			System.err.println(" Example: java SampleMain"+
				argsParser.printExample(OptionHandlerFilter.ALL, null)
			);	
		}
	}

}
