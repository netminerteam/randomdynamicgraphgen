package org.dyminer.jgenerators;

import java.io.FileWriter;
import java.io.IOException;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.dyminer.model.TemporalEdge;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.OptionHandlerFilter;

public class AppMultipleGeneration implements Runnable{
	
	private MultipleGeneratorArgs args;
	
	public AppMultipleGeneration(MultipleGeneratorArgs args) {
		this.args = args;
	}
	
	public void run() {
		FileWriter writer;
		FileWriter gtWriter;
		
		try {
			//we open the file writer
			writer = new FileWriter(this.args.getOutputFile());
			gtWriter = new FileWriter(this.args.getOutputGroundTruthFile());
			
			//we write the header
			writer.write("timestamp, n1, label, n2\n");
			
			//begin preparing the single phase bean
			GeneratorArgs phaseArgs = new GeneratorArgs();
			phaseArgs.maxLabelCount = args.getMaxLabelCount();
			phaseArgs.maxNodeCount = args.getMaxNodeCount();
			phaseArgs.negativePerturbation = args.getNegativePerturbation();
			phaseArgs.positivePerturbation = args.getPositivePerturbation();
			phaseArgs.streamingRate = args.getStreamingRate();

			//we prepare the temporal informations
			Instant startDate = args.getStartDate();
			Instant endDate = startDate.plus(1, ChronoUnit.HOURS);
			Random random = new Random(args.getSeed());
			long lastSeed = 0;
			long currentSeed = 0;
			
			for(int i=0; i<this.args.getPhaseCount(); i++) {
				//we pass the temporal information to the phase arguments
				phaseArgs.startDateTime = startDate.toString().substring(0, endDate.toString().length()-1);
				phaseArgs.endDateTime = endDate.toString().substring(0, endDate.toString().length()-1);
				
				//we select (and remove - selection without replacement) different from the last
				if(!args.withReplacement()) {
					currentSeed = args.getPhaseSeedList().remove(random.nextInt(args.getPhaseSeedList().size()));
				}else{
					currentSeed = args.getPhaseSeedList().get(random.nextInt(args.getPhaseSeedList().size()));
				}
				phaseArgs.seed = currentSeed;
				
				//we generate data for this phase
				System.out.println("beginning phase "+(i+1)+" (of "+this.args.getPhaseCount()+") seed="+currentSeed);
				List<TemporalEdge> edges = GeneratorUtils.singlePassGeneration(phaseArgs).collect(Collectors.toList());
				this.printCSV(writer, edges);
				
				//we save the ground truth
				int blocks = this.args.getStreamingRate()/this.args.getBlockSize();
				for(int b = 0; b<blocks; b++){
					if(b==0) {
						if(i==0) {
							continue;
						}else {
							if(lastSeed==currentSeed) {
								gtWriter.write("0\n");
							}else{
								gtWriter.write("1\n");
							}
						}
					}else {
						gtWriter.write("0\n");
					}
				}
				
				//we replace the last used seed
				if(i>0) {
					if(!args.withReplacement()) {
						args.getPhaseSeedList().add(lastSeed);
					}
				}
				lastSeed = currentSeed;
				
				//we prepare the temporal block for the next phase
				startDate = phaseArgs.getEndDate();
				endDate = startDate.plus(1,  ChronoUnit.HOURS);
			}
			
			writer.close();
			gtWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	private void printCSV(FileWriter writer, List<TemporalEdge> edges) throws IOException{
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:S -0000")
			.withZone(ZoneOffset.UTC);
		
		for(TemporalEdge edge : edges) {
			StringBuilder builder = new StringBuilder();
			builder.append(formatter.format(edge.getTimestamp()));
			builder.append(", ");
			builder.append(edge.getStartNode());
			builder.append(", ");
			builder.append(edge.getLabel());
			builder.append(", ");
			builder.append(edge.getEndNode());
			builder.append("\n");
			writer.write(builder.toString());
		}
	}
	
	
	
	
	/**
	 * Application entry point, in this static method
	 * we perform command-line argument parsing and validation.
	 * If validation succeed, we launch the detector on the designated
	 * data stream.
	 * 
	 * @param args
	 */
	public static void main(String[] args){
		Logger.getLogger("org.hibernate").setLevel(Level.SEVERE);
		final MultipleGeneratorArgs arguments = new MultipleGeneratorArgs();
		final CmdLineParser argsParser = new CmdLineParser(arguments);

		try {
			argsParser.parseArgument(args);
			System.out.println("generating stream {sr="+arguments.getStreamingRate()+
				" (graphs/hour), |V|="+arguments.getMaxNodeCount()+
				", |L|="+arguments.getMaxLabelCount()+"}");
			new AppMultipleGeneration(arguments).run();
		} catch (CmdLineException e) {
			System.err.println(e.getMessage());
			System.err.println("java SampleMain [options...] arguments...");
			argsParser.printUsage(System.err);
			System.err.println();
			System.err.println(" Example: java SampleMain"+
				argsParser.printExample(OptionHandlerFilter.ALL, null)
			);	
		}
	}

}
